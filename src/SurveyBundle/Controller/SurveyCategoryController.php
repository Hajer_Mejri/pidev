<?php

namespace SurveyBundle\Controller;

use SurveyBundle\Entity\SurveyCategory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Surveycategory controller.
 *
 * @Route("surveycategory/admin")
 */
class SurveyCategoryController extends Controller
{
    private $renderPath = 'SurveyBundle:surveycategory:';

    /**
     * Lists all surveyCategory entities.
     *
     * @Route("/", name="surveycategory_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $surveyCategories = $em->getRepository('SurveyBundle:SurveyCategory')->findAll();

        return $this->render($this->renderPath.'index.html.twig', array(
            'surveyCategories' => $surveyCategories,
        ));
    }

    /**
     * Creates a new surveyCategory entity.
     *
     * @Route("/new", name="surveycategory_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $surveyCategory = new Surveycategory();
        $form = $this->createForm('SurveyBundle\Form\SurveyCategoryType', $surveyCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($surveyCategory);
            $em->flush();

            return $this->redirectToRoute('surveycategory_edit', array('id' => $surveyCategory->getId()));
        }

        return $this->render($this->renderPath.'new.html.twig', array(
            'surveyCategory' => $surveyCategory,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a surveyCategory entity.
     *
     * @Route("/{id}", name="surveycategory_show")
     * @Method("GET")
     */
    public function showAction(SurveyCategory $surveyCategory)
    {
        $deleteForm = $this->createDeleteForm($surveyCategory);

        return $this->render($this->renderPath.'show.html.twig', array(
            'surveyCategory' => $surveyCategory,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing surveyCategory entity.
     *
     * @Route("/{id}/edit", name="surveycategory_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SurveyCategory $surveyCategory)
    {
        $deleteForm = $this->createDeleteForm($surveyCategory);
        $editForm = $this->createForm('SurveyBundle\Form\SurveyCategoryType', $surveyCategory);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('surveycategory_edit', array('id' => $surveyCategory->getId()));
        }

        return $this->render($this->renderPath.'edit.html.twig', array(
            'surveyCategory' => $surveyCategory,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a surveyCategory entity.
     *
     * @Route("/{id}", name="surveycategory_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, SurveyCategory $surveyCategory)
    {
        $form = $this->createDeleteForm($surveyCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($surveyCategory);
            $em->flush();
        }

        return $this->redirectToRoute('surveycategory_index');
    }

    /**
     * Creates a form to delete a surveyCategory entity.
     *
     * @param SurveyCategory $surveyCategory The surveyCategory entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SurveyCategory $surveyCategory)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('surveycategory_delete', array('id' => $surveyCategory->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
