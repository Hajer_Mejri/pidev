<?php

namespace SurveyBundle\Controller;

use SurveyBundle\Entity\Survey;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use SurveyBundle\Service\SurveyService;
use Symfony\Component\Security\Core\Security;

/**
 * Survey controller.
 *
 * @Route("sondage/admin")
 */
class SurveyController extends Controller
{
    private $renderPath = 'SurveyBundle:survey:';


    /**
     * @Route("/testSalim", name="testSalim")
     * @Method("GET")
     */
    public function testSalimdAction(Request $httpRequest)
    {
        dump($this->container->getParameter('welp_mailchimp.api_key') );die;
    }

    /**
     * @Route("/surveyDashboard", name="surveyDashboard")
     * @Method("GET")
     */
    public function surveyDashboardAction(Request $httpRequest)
    {
        $em = $this->getDoctrine()->getManager();
        $service = new SurveyService();
        $surveys = $service->calculateForDashboard( $em );

        return $this->render($this->renderPath.'dashboard.html.twig', array(
            'surveys'=>$surveys
        ));
    }

    /**
     * Lists all survey entities.
     *
     * @Route("/", name="sondage_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $surveys = $em->getRepository('SurveyBundle:Survey')->findAll();

        return $this->render($this->renderPath.'index.html.twig', array(
            'surveys' => $surveys,
        ));
    }

    /**
     * Creates a new survey entity.
     *
     * @Route("/new", name="sondage_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $survey = new Survey();
        $form = $this->createForm('SurveyBundle\Form\SurveyType', $survey);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($survey);
            $em->flush();

            return $this->redirectToRoute('sondage_edit', array('id' => $survey->getId()));
        }

        return $this->render($this->renderPath.'new.html.twig', array(
            'survey' => $survey,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a survey entity.
     *
     * @Route("/{id}", name="sondage_show")
     * @Method("GET")
     */
    public function showAction(Survey $survey)
    {
        $deleteForm = $this->createDeleteForm($survey);

        return $this->render($this->renderPath.'show.html.twig', array(
            'survey' => $survey,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing survey entity.
     *
     * @Route("/{id}/edit", name="sondage_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Survey $survey)
    {
        $deleteForm = $this->createDeleteForm($survey);
        $editForm = $this->createForm('SurveyBundle\Form\SurveyType', $survey);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sondage_edit', array('id' => $survey->getId()));
        }

        return $this->render($this->renderPath.'edit.html.twig', array(
            'survey' => $survey,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a survey entity.
     *
     * @Route("/{id}", name="sondage_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Survey $survey)
    {
        $form = $this->createDeleteForm($survey);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($survey);
            $em->flush();
        }

        return $this->redirectToRoute('sondage_index');
    }

    /**
     * Creates a form to delete a survey entity.
     *
     * @param Survey $survey The survey entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Survey $survey)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sondage_delete', array('id' => $survey->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
