<?php

namespace SurveyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use SurveyBundle\Service\ContentService;

class RenderingController extends Controller
{
    public function renderClientTypeFormAction( $form, $entity =null ){
        return $this->render('SurveyBundle:templates:clientTypeForm.html.twig', array(
            'form' => $form,
        )); 
    }
    public function renderSurveyCategoryFormAction( $form, $entity =null ){
        return $this->render('SurveyBundle:templates:surveyCategoryForm.html.twig', array(
            'form' => $form,
        )); 
    }
    public function renderSurveyFormAction( $form, $entity =null ){
        $contents =null;
        if( $entity != null ){
            $service = new ContentService( $this->getDoctrine()->getManager(), $entity );
            $contents = $service->getResult();
        }
        return $this->render('SurveyBundle:templates:surveyForm.html.twig', array(
            'form' => $form,
            'contents' => $contents,
        )); 
    }
}
