<?php
namespace SurveyBundle\Service;

use SurveyBundle\Entity\SurveyResult;

use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Validation;

class SurveyService{
	private $em ;
	public function checkfEmailExist($_email,$survey_id,$em){
		$result = $em->getRepository('SurveyBundle:SurveyResult')->findOneByEmail($_email);
		if( $result == null){
			return false;
		}else{

			if( $result->getSurvey() != $survey_id ){
				return false;
			}
			return true;
		}
	}
	public function validatorBundleFunction($data){
		$validator = Validation::createValidator();
		$firstNameViolations = $validator->validate($data->get('fname'), [
		    new Length(['min' => 5]),
		    new NotBlank(),
		]);
		$lastNameViolations = $validator->validate($data->get('lname'), [
		    new Length(['min' => 5]),
		    new NotBlank(),
		]);
		$emailViolations = $validator->validate($data->get('email'), [
		    new Length(['min' => 5]),
		    new NotBlank(),
		    new Email(),
		]);

		if ( 
			count($firstNameViolations) !==0   && 
			count($lastNameViolations) !==0   && 
			count($emailViolations) !==0   

		) {
		    return 'dataNotvalid';
		}else{
			return 'validData';
		}
	}
	public function saveCondidateData($data,$em){
		$this->updateAnswersScore($data->get('final'),$em);
		$entity = new SurveyResult();
		$entity->setEmail($data->get('email'));
		$entity->setName( $data->get('fname').' '. $data->get('lname'));
		$entity->setSurvey($data->get('survey'));
		$entity->setJson($data->get('final'));
		$em->persist($entity);
		$em->flush(); 
	}
	public function sendMailWithAllData($data,$controller,$mailer ){
		$message = (new \Swift_Message('Hello Email'))
        ->setFrom('send@example.com')
        ->setTo('recipient@example.com')
        ->setBody(
            $controller->renderView(
                'Emails/registration.html.twig',
                ['name' => 'salim']
            ),
            'text/html'
        );
    	$mailer->send($message);

	}
	private function addNewSubscriber($email,$fname,$lname,$api){

	}
	public function calculateForDashboard($em){
		$this->em = $em;
		$finalArray = array();
		$surveys = $em->getRepository('SurveyBundle:Survey')->findAll();
		foreach( $surveys as $survey ){
			$clientAnswers = $this->em->getRepository('SurveyBundle:SurveyResult')->findBy(	
				array(
					'survey'=>$survey->getId()
				),
				array()	
			);
			$finalArray[] = array(
				'title'=>$survey->getTitle(),
				'countOfAcceptedAnswers' => sizeof($clientAnswers),
				'questions'=> $this->getAnswersByQuestion($survey,$clientAnswers,$this->em)
			);
		}
		return $finalArray;
	}
	private function getAnswersByQuestion($survey,$clientAnswers,$em ){
		$finalArray = array();
		foreach ($survey->getQuestions() as $question) {
			$finalArray[]  = array(
				'content'=>$question->getContent(),
				'answers' => $this->getAnswers( $question->getAnswers() ),
				'bestAnswer' => $this-> getBestAnswer( $this->getAnswers( $question->getAnswers() ) )
			);
		}
		return $finalArray;
	}
	private function getBestAnswer( $answers ){
		$bestScore = 0;
		$result = '';
		foreach ($answers as $answer) {
			if( $answer['score'] > $bestScore ){
				$result = $answer['content'];
			}
		}
		return $result;
	}
	private function getAnswers($answers){
		$finalArray = array();
		foreach ($answers as $answer) {
			$finalArray[] = array(
				'content'=>$answer->getContent(),
				'score'=>$answer->getScore()
			);
		}
		return $finalArray;
	}
	private function updateAnswersScore($json,$em){
		$decoedJson = $decodedArray = json_decode($json,true);
		for ($i=0; $i < sizeof( $decodedArray ) ; $i++) { 
			$answer = $em->getRepository('SurveyBundle:Answers')->findOneById( $decodedArray[$i]['answer_id'] );
			$answer->setScore( $answer->getScore() + 1 );
			$em->flush();
		}
	}
	public function getAllQuestions( $element ){
		$finalArray  = array();
		foreach ($element->getQuestions() as $question) {
			$finalArray[] = array(
				'content' => $question->getContent(),
				'answers' => $this->getAllAnswers( $question ),
			);
		}
		return $finalArray;
	}
	private function getAllAnswers( $element ){
		$finalArray = array();
		foreach ($element->getAnswers() as $answer ) {
			$finalArray[] = array (
				'content'=> $answer->getContent()
			);
		}
		return $finalArray;
	}
}
