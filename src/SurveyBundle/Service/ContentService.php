<?php
namespace SurveyBundle\Service;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ContentService
{
	private $em;
	private $result; 

	public function __construct($em,$entity) {  
		$this->em = $em;
		$this->result = $this->whichContent($entity);
  }
  private function whichContent( $entity ){
   		$dataArray = array();

   		//textBloc
   		if ( method_exists($entity,'getQuestions' )  ){
   			foreach ($entity->getQuestions() as $question) {
              $dataArray[] =  array(
                  'type' => 'question', 
                  'entity' => $question, 
                  'position' => $question->getPosition() 
              );
          }
   		}
   		/** order by position **/
      $dataArray = $this->setPositionOrder($dataArray);

      return $dataArray;
   	}
  private function setPositionOrder($array){
      $sortArray = array(); 
      if(isset($array) && sizeof( $array ) > 0  ){
          foreach($array as $person){ 
              foreach($person as $key=>$value){ 
                  if(!isset($sortArray[$key])){ 
                      $sortArray[$key] = array(); 
                  } 
                  $sortArray[$key][] = $value; 
              } 
          } 
          $orderby = "position";
          array_multisort($sortArray[$orderby],SORT_ASC,$array); 
      }
      return $array;
    }

   public function getResult(){
   		return $this->result;
   }
}
