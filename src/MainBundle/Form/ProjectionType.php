<?php

namespace MainBundle\Form;

use Doctrine\ORM\EntityRepository;
use MainBundle\Entity\Salle;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\Tests\Extension\EnableableExtension;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ProjectionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descriptionProjection', null, array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'descriptionProjection'
                )
            ))
            ->add('categorieProjection',ChoiceType::class,
                array(
                    "choices"=>array(
                        ""=>"",
                        "FILM"=>"FILM",
                        "THEATRE"=>"THEATRE",

                    ),

                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'categorieProjection'
                    )
                ))
            ->add('titleProjection', null, array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'titleProjection'
                )
            ))
            ->add('dureeProjection', null, array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'dureeProjection'
                )
            ))
            ->add('dateProjection', DateType::class, array(
                'widget' => 'single_text',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'dateProjection'
                )
            ))
            ->add('prixProjection', null, array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'prixProjection'
                )
            ))->add('salle',
                EntityType::class,
                [
                    'placeholder' => 'Choisir une salle',
                    'required' => true,
                    'class' => Salle::class,
                    'choice_label' => 'nomSalle' ,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('salle')
                            ->where('salle.Etat_salle = :n')
                            ->setParameter('n', 'DISPONIBLE')
                            ->orderBy('salle.id');
                    },
                    //'multiple' => true,
                    //'expanded' => true,
                ]
            )
            ->add('file', FileType::class, array('required' => false, 'label' => 'Image'))
            ->add('Ajouter', SubmitType::class);


    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Projection'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mainbundle_projection';
    }


}
