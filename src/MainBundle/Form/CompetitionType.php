<?php

namespace MainBundle\Form;

use MainBundle\Entity\Club;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompetitionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nameCompetition')
            ->add('visibilityCompetition')
            ->add('startDateCompetition', DateTimeType::class)
            ->add('endDateCompetition', DateTimeType::class)
            ->add('descriptionCompetition')
            //->add('file', FileType::class, array('required' => true,'label'=>'Image'))
            ->add('placeCompetition')
            ->add('contactCompetition')
            ->add('club',
                EntityType::class,
                [
                    'class' => Club::class,
                    'choice_label' => 'name_club',
                    //'multiple' => true,
                    //'expanded' => true,
                ]
            );

        // evenement eventListener
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $competition = $event->getData();
            $form = $event->getForm();

            // checks if the Competition object is "new"
            // If no data is passed to the form, the data is "null".
            // This should be considered a new "Compitition"
            if (!$competition || null === $competition->getId()) {
                $form->add('file', FileType::class, array('required' => true, 'label' => 'Image'));
            } else {
                $form->add('file', FileType::class, array('required' => false,'label' => 'Image'));
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Competition'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mainbundle_competition';
    }


}
