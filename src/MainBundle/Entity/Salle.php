<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Salle
 *
 * @ORM\Table(name="salle")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\SalleRepository")
 */
class Salle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     *
     * @ORM\Column(name="Nom_salle", type="string", length=255)
     */
    private $nomSalle;

    /**
     * @var string
     *
     * @ORM\Column(name="Etat_salle", type="string", length=255)
     */
    private $Etat_salle;

    /**
     * @var string
     *
     * @ORM\Column(name="Bloc_salle", type="string", length=255)
     */
    private $blocSalle;

    /**
     * @var int
     *
     * @ORM\Column(name="NbreChaise_salle", type="integer")
     */
    private $nbreChaiseSalle;

    /**
     * @var string
     *
     * @ORM\Column(name="Categorie_salle", type="string", length=255)
     */
    private $categorieSalle;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->projections = new \Doctrine\Common\Collections\ArrayCollection();
    }
    public function __toString()
    {
        return $this->getNomSalle();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomSalle
     *
     * @param string $nomSalle
     *
     * @return Salle
     */
    public function setNomSalle($nomSalle)
    {
        $this->nomSalle = $nomSalle;

        return $this;
    }

    /**
     * Get nomSalle
     *
     * @return string
     */
    public function getNomSalle()
    {
        return $this->nomSalle;
    }

    /**
     * Set etatSalle
     *
     * @param string $etatSalle
     *
     * @return Salle
     */
    public function setEtatSalle($etatSalle)
    {
        $this->Etat_salle = $etatSalle;

        return $this;
    }

    /**
     * Get etatSalle
     *
     * @return string
     */
    public function getEtatSalle()
    {
        return $this->Etat_salle;
    }

    /**
     * Set blocSalle
     *
     * @param string $blocSalle
     *
     * @return Salle
     */
    public function setBlocSalle($blocSalle)
    {
        $this->blocSalle = $blocSalle;

        return $this;
    }

    /**
     * Get blocSalle
     *
     * @return string
     */
    public function getBlocSalle()
    {
        return $this->blocSalle;
    }

    /**
     * Set nbreChaiseSalle
     *
     * @param integer $nbreChaiseSalle
     *
     * @return Salle
     */
    public function setNbreChaiseSalle($nbreChaiseSalle)
    {
        $this->nbreChaiseSalle = $nbreChaiseSalle;

        return $this;
    }

    /**
     * Get nbreChaiseSalle
     *
     * @return integer
     */
    public function getNbreChaiseSalle()
    {
        return $this->nbreChaiseSalle;
    }

    /**
     * Set categorieSalle
     *
     * @param string $categorieSalle
     *
     * @return Salle
     */
    public function setCategorieSalle($categorieSalle)
    {
        $this->categorieSalle = $categorieSalle;

        return $this;
    }

    /**
     * Get categorieSalle
     *
     * @return string
     */
    public function getCategorieSalle()
    {
        return $this->categorieSalle;
    }
}
