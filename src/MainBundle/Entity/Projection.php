<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Projection
 *
 * @ORM\Table(name="projection")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ProjectionRepository")
 */
class Projection
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Description_Projection", type="string", length=255)
     */
    private $descriptionProjection;

    /**
     * @var string
     *
     * @ORM\Column(name="Categorie_Projection", type="string", length=255)
     */
    private $categorieProjection;

    /**
     * @var string
     *
     * @ORM\Column(name="Title_Projection", type="string", length=255)
     */
    private $titleProjection;

    /**
     * @var string
     *
     * @ORM\Column(name="Duree_Projection", type="string", length=255)
     */
    private $dureeProjection;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_Projection", type="date")
     */
    private $dateProjection;

    /**
     * @var int
     *
     * @ORM\Column(name="Prix_Projection", type="integer")
     */
    private $prixProjection;


    /**
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Salle", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $salle;

    /**
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\MediaP", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $image;

    public $file;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descriptionProjection
     *
     * @param string $descriptionProjection
     *
     * @return Projection
     */
    public function setDescriptionProjection($descriptionProjection)
    {
        $this->descriptionProjection = $descriptionProjection;

        return $this;
    }

    /**
     * Get descriptionProjection
     *
     * @return string
     */
    public function getDescriptionProjection()
    {
        return $this->descriptionProjection;
    }

    /**
     * Set categorieProjection
     *
     * @param string $categorieProjection
     *
     * @return Projection
     */
    public function setCategorieProjection($categorieProjection)
    {
        $this->categorieProjection = $categorieProjection;

        return $this;
    }

    /**
     * Get categorieProjection
     *
     * @return string
     */
    public function getCategorieProjection()
    {
        return $this->categorieProjection;
    }

    /**
     * Set titleProjection
     *
     * @param string $titleProjection
     *
     * @return Projection
     */
    public function setTitleProjection($titleProjection)
    {
        $this->titleProjection = $titleProjection;

        return $this;
    }

    /**
     * Get titleProjection
     *
     * @return string
     */
    public function getTitleProjection()
    {
        return $this->titleProjection;
    }

    /**
     * Set dureeProjection
     *
     * @param string $dureeProjection
     *
     * @return Projection
     */
    public function setDureeProjection($dureeProjection)
    {
        $this->dureeProjection = $dureeProjection;

        return $this;
    }

    /**
     * Get dureeProjection
     *
     * @return string
     */
    public function getDureeProjection()
    {
        return $this->dureeProjection;
    }

    /**
     * Set dateProjection
     *
     * @param \DateTime $dateProjection
     *
     * @return Projection
     */
    public function setDateProjection($dateProjection)
    {
        $this->dateProjection = $dateProjection;

        return $this;
    }

    /**
     * Get dateProjection
     *
     * @return \DateTime
     */
    public function getDateProjection()
    {
        return $this->dateProjection;
    }

    /**
     * Set prixProjection
     *
     * @param integer $prixProjection
     *
     * @return Projection
     */
    public function setPrixProjection($prixProjection)
    {
        $this->prixProjection = $prixProjection;

        return $this;
    }

    /**
     * Get prixProjection
     *
     * @return integer
     */
    public function getPrixProjection()
    {
        return $this->prixProjection;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return mixed
     */
    public function getSalle()
    {
        return $this->salle;
    }

    /**
     * @param mixed $salle
     */
    public function setSalle($salle)
    {
        $this->salle = $salle;
    }

    /**
     * Get image
     *
     * @return \MainBundle\Entity\MediaP
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set image
     *
     * @param MediaP $image
     *
     * @return Projection
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getThumb()
    {
        if ($this->image instanceof MediaP) {
            return $this->image->getPath();
        } else {
            return "noimage/noimage.png";
        }
    }

    public function getImageFileName()
    {
        if ($this->file instanceof UploadedFile) {
            $filename = $this->file->getClientOriginalName();
            $pieces = explode('.', $filename);
            $name = reset($pieces);
            return $name;
        }
        return false;
    }
}
