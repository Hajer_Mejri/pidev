<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * Club
 *
 * @ORM\Table(name="club")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ClubRepository")
 */
class Club
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name_club", type="string", length=50, nullable=true)
     */
    private $nameClub;

    /**
     * @var string
     *
     * @ORM\Column(name="category_club", type="string", length=100, nullable=true)
     */
    private $categoryClub;

    /**
     * @var string
     *
     * @ORM\Column(name="activity_club", type="string", length=100, nullable=true)
     */
    private $activityClub;

    /**
     * @var string
     *
     * @ORM\Column(name="description_club", type="text", nullable=true)
     */
    private $descriptionClub;

    /**
     * @var float
     *
     * @ORM\Column(name="price_club", type="float", nullable=true)
     */
    private $priceClub;

    /**
     * @var string
     *
     * @ORM\Column(name="openningHours_club", type="string", length=255, nullable=true)
     */
    private $openningHoursClub;

    /**
     * @var string
     *
     * @ORM\Column(name="closingHours_club", type="string", length=255, nullable=true)
     */
    private $closingHoursClub;

    /**
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\Media", mappedBy="club", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $images;

    /**
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\ClubVoucher", mappedBy="club", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $vouchers;

    /**
     * @var float
     *
     * @ORM\Column(name="sum_vote", type="float", nullable=true)
     */
    private $sum_vote;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_vote", type="integer", nullable=true)
     */
    private $nb_vote;

    /**
     * @var text
     *
     * @ORM\Column(name="voters", type="text", nullable=true)
     */
    private $voters;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Add image
     *
     * @param \MainBundle\Entity\Media $media
     *
     * @return Club
     */
    public function addImage(Media $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \MainBundle\Entity\Media $media
     */
    public function removeImage(Media $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add voucher
     *
     * @param \MainBundle\Entity\ClubVoucher $voucher
     *
     * @return Club
     */
    public function addVoucher(ClubVoucher $voucher)
    {
        $this->vouchers[] = $voucher;

        return $this;
    }

    /**
     * Remove voucher
     *
     * @param \MainBundle\Entity\ClubVoucher $voucher
     */
    public function removeVoucher(ClubVoucher $voucher)
    {
        $this->vouchers->removeElement($voucher);
    }

    /**
     * Get vouchers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVouchers()
    {
        return $this->vouchers;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameClub
     *
     * @param string $nameClub
     *
     * @return Club
     */
    public function setNameClub($nameClub)
    {
        $this->nameClub = $nameClub;

        return $this;
    }

    /**
     * Get nameClub
     *
     * @return string
     */
    public function getNameClub()
    {
        return $this->nameClub;
    }

    /**
     * Set categoryClub
     *
     * @param string $categoryClub
     *
     * @return Club
     */
    public function setCategoryClub($categoryClub)
    {
        $this->categoryClub = $categoryClub;

        return $this;
    }

    /**
     * Get categoryClub
     *
     * @return string
     */
    public function getCategoryClub()
    {
        return $this->categoryClub;
    }

    /**
     * Set activityClub
     *
     * @param string $activityClub
     *
     * @return Club
     */
    public function setActivityClub($activityClub)
    {
        $this->activityClub = $activityClub;

        return $this;
    }

    /**
     * Get activityClub
     *
     * @return string
     */
    public function getActivityClub()
    {
        return $this->activityClub;
    }

    /**
     * Set priceClub
     *
     * @param float $priceClub
     *
     * @return Club
     */
    public function setPriceClub($priceClub)
    {
        $this->priceClub = $priceClub;

        return $this;
    }

    /**
     * Get priceClub
     *
     * @return float
     */
    public function getPriceClub()
    {
        return $this->priceClub;
    }

    /**
     * Set openningHoursClub
     *
     * @param string $openningHoursClub
     *
     * @return Club
     */
    public function setOpenningHoursClub($openningHoursClub)
    {
        $this->openningHoursClub = $openningHoursClub;

        return $this;
    }

    /**
     * Get openningHoursClub
     *
     * @return string
     */
    public function getOpenningHoursClub()
    {
        return $this->openningHoursClub;
    }

    /**
     * Set closingHoursClub
     *
     * @param string $closingHoursClub
     *
     * @return Club
     */
    public function setClosingHoursClub($closingHoursClub)
    {
        $this->closingHoursClub = $closingHoursClub;

        return $this;
    }

    /**
     * Get closingHoursClub
     *
     * @return string
     */
    public function getClosingHoursClub()
    {
        return $this->closingHoursClub;
    }

    /**
     * Get descriptionClub
     *
     * @return string
     */
    public function getDescriptionClub()
    {
        return $this->descriptionClub;
    }

    /**
     * Set descriptionClub
     *
     * @param string $descriptionClub
     *
     * @return Club
     */
    public function setDescriptionClub($descriptionClub)
    {
        $this->descriptionClub = $descriptionClub;
    }

    /**
     * @return float
     */
    public function getSumVote()
    {
        return $this->sum_vote;
    }

    /**
     * @param float $sum_vote
     */
    public function addSumVote($sum_vote)
    {
        $this->sum_vote += $sum_vote;
    }


    /**
     * @return int
     */
    public function getNbVote()
    {
        return $this->nb_vote;
    }

    /**
     * @param int $nb_vote
     */
    public function incrementNbVote()
    {
        $this->nb_vote++;
    }

    public function addVoter($voter)
    {
        if ($voter instanceof User) {
            if ($this->voters) {
                $voters = json_decode($this->voters);
                if (!in_array($voter->getId(), $voters)) {
                    $voters[] = $voter->getId();
                }
            } else {
                $voters = array();
                $voters[] = $voter->getId();
            }
            $this->voters = json_encode($voters);
        }
        return $this;
    }

    public function oneVote($voter)
    {
        if ($this->voters) {
            if ($voter instanceof User) {
                $voters = json_decode($this->voters);
                if (in_array($voter->getId(), $voters)) {
                    return false;
                }
            }
        }
        return true;
    }

    public function getRatingstar()
    {
        if ($this->sum_vote && $this->nb_vote != 0) {
            return $this->sum_vote / $this->nb_vote;
        }
        return false;
    }

    public function getFirstThumb()
    {
        if ($this->images[0] instanceof Media) {
            return $this->images[0]->getPath();
        } else {
            return "noimage/noimage.png";
        }
    }
}

