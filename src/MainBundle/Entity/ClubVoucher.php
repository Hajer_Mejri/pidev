<?php

namespace MainBundle\Entity;

use ContainerVmyeruh\appDevDebugProjectContainer;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * ClubVoucher
 *
 * @ORM\Table(name="club_voucher")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ClubVoucherRepository")
 */
class ClubVoucher
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Club", inversedBy="vouchers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $club;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="vouchers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=false)
     */
    private $code;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="insertat", type="datetime", nullable=true)
     */
    private $insertat;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * @param mixed $club
     */
    public function setClub($club)
    {
        $this->club = $club;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return \DateTime
     */
    public function getInsertat()
    {
        return $this->insertat;
    }

    /**
     * @param \DateTime $insertat
     */
    public function setInsertat($insertat)
    {
        $this->insertat = $insertat;
    }

    public function getDateExpired()
    {
        if ($this->insertat) {
            $date = $this->insertat;
            date_add($this->insertat, date_interval_create_from_date_string('15 days'));
            return date_format($date, 'm/d/Y');
        }
        return false;
    }
}

