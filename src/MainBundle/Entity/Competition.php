<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Competition
 *
 * @ORM\Table(name="competition")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\CompetitionRepository")
 */
class Competition
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name_competition", type="string", length=50, nullable=true)
     */
    private $nameCompetition;

    /**
     * @var bool
     *
     * @ORM\Column(name="visibility_competition", type="boolean")
     */
    private $visibilityCompetition;

    /**
     * @var datetime
     *
     * @ORM\Column(name="startDate_competition", type="datetime", nullable=true)
     */
    private $startDateCompetition;

    /**
     * @var datetime
     *
     * @ORM\Column(name="endDate_competition", type="datetime", nullable=true)
     */
    private $endDateCompetition;

    /**
     * @var string
     *
     * @ORM\Column(name="description_competition", type="text", nullable=true)
     */
    private $descriptionCompetition;

    /**
     * @var string
     *
     * @ORM\Column(name="place_competition", type="text", nullable=true)
     */
    private $placeCompetition;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_competition", type="text", nullable=true)
     */
    private $contactCompetition;


    /**
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Club", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $club;

    /**
     * @ORM\ManyToOne(targetEntity="MainBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $media;

    public $file;

    /**
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\CompetitionUser", mappedBy="competition", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $demandes;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="insertat", type="datetime", nullable=true)
     */
    private $insertat;

    /**
     * Add demande
     *
     * @param \MainBundle\Entity\CompetitionUser $demande
     *
     * @return User
     */
    public function addDemandeCompetition(CompetitionUser $demande)
    {
        $this->demandes[] = $demande;

        return $this;
    }

    /**
     * Remove demande
     *
     * @param \MainBundle\Entity\CompetitionUser $demande
     */
    public function removeDemandeCompetition(CompetitionUser $demande)
    {
        $this->demandes->removeElement($demande);
    }

    /**
     * Get demandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDemandesCompetitions()
    {
        return $this->demandes;
    }

    /**
     * @return \DateTime
     */
    public function getInsertat()
    {
        return $this->insertat;
    }

    /**
     * @param \DateTime $insertat
     */
    public function setInsertat($insertat)
    {
        $this->insertat = $insertat;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return mixed
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @param mixed $media
     */
    public function setMedia($media)
    {
        $this->media = $media;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nameCompetition
     *
     * @param string $nameCompetition
     *
     * @return Competition
     */
    public function setNameCompetition($nameCompetition)
    {
        $this->nameCompetition = $nameCompetition;

        return $this;
    }

    /**
     * Get nameCompetition
     *
     * @return string
     */
    public function getNameCompetition()
    {
        return $this->nameCompetition;
    }

    /**
     * Set visibilityCompetition
     *
     * @param boolean $visibilityCompetition
     *
     * @return Competition
     */
    public function setVisibilityCompetition($visibilityCompetition)
    {
        $this->visibilityCompetition = $visibilityCompetition;

        return $this;
    }

    /**
     * Get visibilityCompetition
     *
     * @return bool
     */
    public function getVisibilityCompetition()
    {
        return $this->visibilityCompetition;
    }

    /**
     * Set startDateCompetition
     *
     * @param string $startDateCompetition
     *
     * @return Competition
     */
    public function setStartDateCompetition($startDateCompetition)
    {
        $this->startDateCompetition = $startDateCompetition;

        return $this;
    }

    /**
     * Get startDateCompetition
     *
     * @return string
     */
    public function getStartDateCompetition()
    {
        return $this->startDateCompetition;
    }

    /**
     * Set endDateCompetition
     *
     * @param string $endDateCompetition
     *
     * @return Competition
     */
    public function setEndDateCompetition($endDateCompetition)
    {
        $this->endDateCompetition = $endDateCompetition;

        return $this;
    }

    /**
     * Get endDateCompetition
     *
     * @return string
     */
    public function getEndDateCompetition()
    {
        return $this->endDateCompetition;
    }

    /**
     * Set descriptionCompetition
     *
     * @param string $descriptionCompetition
     *
     * @return Competition
     */
    public function setDescriptionCompetition($descriptionCompetition)
    {
        $this->descriptionCompetition = $descriptionCompetition;

        return $this;
    }

    /**
     * Get descriptionCompetition
     *
     * @return string
     */
    public function getDescriptionCompetition()
    {
        return $this->descriptionCompetition;
    }

    /**
     * Set placeCompetition
     *
     * @param string $placeCompetition
     *
     * @return Competition
     */
    public function setPlaceCompetition($placeCompetition)
    {
        $this->placeCompetition = $placeCompetition;

        return $this;
    }

    /**
     * Get placeCompetition
     *
     * @return string
     */
    public function getPlaceCompetition()
    {
        return $this->placeCompetition;
    }

    /**
     * Set contactCompetition
     *
     * @param string $contactCompetition
     *
     * @return Competition
     */
    public function setContactCompetition($contactCompetition)
    {
        $this->contactCompetition = $contactCompetition;

        return $this;
    }

    /**
     * Get contactCompetition
     *
     * @return string
     */
    public function getContactCompetition()
    {
        return $this->contactCompetition;
    }

    /**
     * Get club
     *
     * @return \MainBundle\Entity\Club
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * Set club
     *
     * @param \MainBundle\Entity\Club $club
     *
     * @return Competition
     */
    public function setClub($club)
    {
        $this->club = $club;
    }

    public function getCountdown()
    {
        $nowDate = new \DateTime();
        $formule = null;

        if (($nowDate) < $this->startDateCompetition) {
            $diff = $nowDate->diff($this->startDateCompetition);
            $days = $diff->d * 24 * 60 * 60 * 1000;
            $hours = $diff->h * 60 * 60 * 1000;
            $mins = $diff->i * 60 * 1000;
            $secs = $diff->s * 1000;
            $formule = $days + $hours + $mins + $secs;
        }
        return $formule;
    }

    public function getCountNewDemandes()
    {
        $i = 0;
        foreach ($this->demandes as $demande){
            if($demande->getStatus() == 0){
                $i++;
            }
        }
        return $i;
    }
}

