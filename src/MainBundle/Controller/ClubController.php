<?php

namespace MainBundle\Controller;

use HttpResponse;
use MainBundle\Entity\Club;
use MainBundle\Entity\Media;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ClubController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $clubs = $em->getRepository('MainBundle:Club')->findAll();
        return $this->render('@Main/club/index.html.twig', array(
            'clubs' => $clubs
        ));
    }

    public function newAction(Request $request)
    {
        $club = new Club();
        $form = $this->createForm('MainBundle\Form\ClubType', $club);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $club->setUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($club);
            $em->flush($club);
            return $this->redirectToRoute('club_media', array('id' => $club->getId()));

        }

        return $this->render('@Main/Club/new.html.twig', array(
            'club' => $club,
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $club = $em->getRepository('MainBundle:Club')->find($id);
        if (!$club instanceof Club) {
            throw $this->createNotFoundException('La page n\'existe pas.');
        }
        $editform = $this->createForm('MainBundle\Form\ClubType', $club);
        $editform->handleRequest($request);

        if ($editform->isSubmitted() && $editform->isValid()) {
            $em->flush($club);

            return $this->redirectToRoute('club_index');
        }
        return $this->render('@Main/Club/edit.html.twig', array(
            'club' => $club,
            'form' => $editform->createView(),
        ));


    }

    public function deleteAction($id)
    {
        if ($id > 0) {
            $em = $this->getDoctrine()->getManager();
            $club = $em->getRepository('MainBundle:Club')->find($id);
            if (!$club instanceof Club)
                throw $this->createNotFoundException('La page n\'existe pas.');


            $em->remove($club);
            $em->flush($club);
            return $this->redirectToRoute('club_index');

        } else {
            throw $this->createNotFoundException('La page n\'existe pas.');
        }

    }

    public function uploadAction($id,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $club = $em->getRepository('MainBundle:Club')->find($id);
        if (!$club instanceof Club) {
            throw $this->createNotFoundException('La page n\'existe pas.');
        }

        //creation d'un form
        $media = new Media();
        $form = $this->createForm('MainBundle\Form\MediaType', $media);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $file = $media->getFile();

            // generation d'un path unique et automatique de l'image
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            //insertion de l'image sous le dossier web/uploads
            $file->move($this->getParameter('upload_directory'), $fileName);

            $media->setPath($fileName);
            $media->setClub($club);

            //insertion date d'upload
            $media->setInsertat(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($media);
            $em->flush($media);
            return $this->redirectToRoute('club_media', array('id' => $id));
        }

        return $this->render('@Main/Club/upload.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function mediaAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $images = $em->getRepository('MainBundle:Media')->findBy(array('club' => $id));
        return $this->render('@Main/club/media.html.twig', array(
            'images' => $images,
            'id' => $id,
        ));
    }

    public function mediaAddMultiAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $club = $em->getRepository('MainBundle:Club')->find($id);
        if (!$club instanceof Club) {
            throw $this->createNotFoundException('La page n\'existe pas.');
        }
        return $this->render('@Main/Club/mediaAddMulti.html.twig', array(
            'id' => $id,
        ));
    }

    public function uploadMultiAction($id)
    {
        if (!empty($_FILES) && $id > 0) {
            $em = $this->getDoctrine()->getManager();
            $club = $em->getRepository('MainBundle:Club')->find($id);
            $filesArray = $_FILES;
            foreach ($filesArray as $file) {
                if (in_array($file['type'], ['bmp' => 'image/bmp',
                        'gif' => 'image/gif',
                        'jpe' => 'image/jpeg',
                        'jpeg' => 'image/jpeg',
                        'jpg' => 'image/jpeg',
                        'png' => 'image/png',
                        'tiff' => 'image/tiff',
                        'tif' => 'image/tiff',]
                )) {
                    $image = new Media();

                    $path = $this->getPathImage($file);
                    $image->setPath($path);
                    $image->setName($file['name']);
                    $image->setInsertat(new \DateTime());
                    $image->setClub($club);

                    $em->persist($image);
                    $em->flush($image);

                    if ($image->getId() > 0) {
                        $tmp_name = $file["tmp_name"];
                        move_uploaded_file($tmp_name, $this->getParameter('upload_directory') . "/" . $path);
                    }

                } else {
                    throw new \Exception('Erreur');
                }
            }

            $response = new JsonResponse();
            return $response->setData(array('club' => $club));
        }
    }

    protected function getPathImage($file)
    {
        $array = explode('.', $file['name']);
        $extension = end($array);
        $path = md5(uniqid()) . '.' . $extension;
        return $path;
    }

    public function mediadeleteAction($id)
    {
        if ($id > 0) {
            $em = $this->getDoctrine()->getManager();
            $media = $em->getRepository('MainBundle:Media')->find($id);
            if (!$media instanceof Media)
                throw $this->createNotFoundException('La page n\'existe pas.');

            $idclub = $media->getClub()->getId();
            if (file_exists($this->getParameter('upload_directory').'/'.$media->getPath())) {
                unlink($this->getParameter('upload_directory').'/'.$media->getPath());
            } else {
                throw $this->createNotFoundException('La page n\'existe pas.');
            }
            $em->remove($media);
            $em->flush($media);
            return $this->redirectToRoute('club_media', array('id' => $idclub));

        } else {
            throw $this->createNotFoundException('La page n\'existe pas.');
        }

    }
}
