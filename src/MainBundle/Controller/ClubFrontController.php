<?php

namespace MainBundle\Controller;

use HttpResponse;
use MainBundle\Entity\Club;
use MainBundle\Entity\ClubVoucher;
use MainBundle\Entity\Media;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

class ClubFrontController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $clubs = $em->getRepository('MainBundle:Club')->findAll();
        return $this->render('@Main/clubfront/index.html.twig', array(
            'clubs' => $clubs
        ));
    }

    public function showAction($id)
    {
        if ($id > 0) {
            $em = $this->getDoctrine()->getManager();
            $club = $em->getRepository('MainBundle:Club')->find($id);
            return $this->render('@Main/clubfront/detailsClub.html.twig', array(
                'club' => $club
            ));
        }
    }

    public function voucherAction($id)
    {
        //recuperation de user connecté
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($id > 0 && $user instanceof User) {
            $em = $this->getDoctrine()->getManager();
            $club = $em->getRepository('MainBundle:Club')->find($id);
            $voucher = $em->getRepository('MainBundle:ClubVoucher')->findOneBy(
                array('user' => $user->getId(), 'club' => $club->getId())
            );
            // si le voucher n'est pas encore généré
            if (!$voucher instanceof ClubVoucher) {
                $voucher = new ClubVoucher();
                $voucher->setClub($club);
                $voucher->setUser($user);
                $voucher->setInsertat(new \DateTime());
                //generation code voucher automatiquement
                $code = sha1(uniqid());
                $voucher->setCode($code);
                $em->persist($voucher);
                $em->flush($voucher);
            }
            // sinon on récupere le voucher existant déjà
            return $this->render('@Main/clubfront/voucher.html.twig', array(
                'voucher' => $voucher
            ));
        } else {
            throw $this->createNotFoundException('La page n\'existe pas.');
        }
    }

    public function rateAction($id, Request $request)
    {
        $rating = $request->request->get('data-rating');
        if ($rating) {
            $em = $this->getDoctrine()->getManager();
            $club = $em->getRepository('MainBundle:Club')->find($id);
            $user = $this->get('security.token_storage')->getToken()->getUser();
            if ($club instanceof Club && $user instanceof User) {
                $club->incrementNbVote();
                $club->addSumVote($rating);
                $club->addVoter($user);
                $em->persist($club);
                $em->flush($club);
                $response = new JsonResponse();
                return $response->setData(array('rating' => $club->getRatingstar()));
            }
        }
        $response = new JsonResponse();
        return $response->setData(array('rating' => false));
    }
}
