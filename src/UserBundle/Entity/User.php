<?php
namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */

class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255, nullable=true)
     */
    private $Nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Prenom", type="string", length=255, nullable=true)
     */
    private $Prenom;

     /**
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\ClubVoucher", mappedBy="user", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $vouchers;

    /**
     * @ORM\OneToMany(targetEntity="MainBundle\Entity\CompetitionUser", mappedBy="user", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $demandes;

    /**
     * Add demande
     *
     * @param \MainBundle\Entity\CompetitionUser $demande
     *
     * @return User
     */
    public function addDemandeCompetition(CompetitionUser $demande)
    {
        $this->demandes[] = $demande;

        return $this;
    }

    /**
     * Remove demande
     *
     * @param \MainBundle\Entity\CompetitionUser $demande
     */
    public function removeDemandeCompetition(CompetitionUser $demande)
    {
        $this->demandes->removeElement($demande);
    }

    /**
     * Get demandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDemandesCompetitions()
    {
        return $this->demandes;
    }

    /**
     * Add voucher
     *
     * @param \MainBundle\Entity\ClubVoucher $voucher
     *
     * @return User
     */
    public function addVoucher(ClubVoucher $voucher)
    {
        $this->vouchers[] = $voucher;

        return $this;
    }

    /**
     * Remove voucher
     *
     * @param \MainBundle\Entity\ClubVoucher $voucher
     */
    public function removeVoucher(ClubVoucher $voucher)
    {
        $this->vouchers->removeElement($voucher);
    }

    /**
     * Get vouchers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVouchers()
    {
        return $this->vouchers;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Get Nom
     *
     * @return Nom
     */

    public function getNom()
    {
        return $this->Nom;
    }

    /**
     * Get Prenom
     *
     * @return Prenom
     */

    public function getPrenom()
    {
        return $this->Prenom;
    }

    public function setNom($Nom)
    {
        $this->Nom = $Nom;

        return $this;
    }
    public function setPrenom($Prenom)
    {
        $this->Prenom = $Prenom;

        return $this;
    }
    public function __construct()
    {
    parent  ::__construct();
    // your own logic
    }
    public function getFullname(){
        return $this->Nom." ".$this->Prenom;
    }
}

