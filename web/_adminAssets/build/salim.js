$(document).ready(function() {


    // function all_articles_function(content) {
    //     $.ajax({
    //         type: 'GET',
    //         url: all_articles_url,
    //         dataType: 'json',
    //         success: function(data) {
    //             var result;
    //             for (var i = 0; i < data.articles.length; i++) {
    //                 result = result + '<option value="' + data.articles[i].slug + '">' + data.articles[i].title + '</option>';
    //             }
    //             content.find('.articles_all').append(result)
    //         },
    //         error: function(result) {}
    //     })
    // }

    // function all_pages_function(content) {
    //     $.ajax({
    //         type: 'GET',
    //         url: all_articles_url,
    //         dataType: 'json',
    //         success: function(data) {
    //             var result;
    //             for (var i = 0; i < data.pages.length; i++) {
    //                 result = result + '<option value="' + data.pages[i].slug + '">' + data.pages[i].title + '</option>';
    //             }
    //             content.find('.pages_all').append(result)
    //         },
    //         error: function(result) {}
    //     })
    // }

    $('.deleteEntity').click(function(event) {
        var person = prompt("Confirmer l'action en tappant DELETE ");
        if (person === 'DELETE') {
            return true
        } else {
            alert('action non validé')
            return false
        }

    });


    $('.validBtn').click(function() {
        if ($(".thumbleImportant").get(0).files.length === 0 && $("html").find('.imgVingFortest') == false) {
            $('body').find('.redimportant').find('h2').css('color', 'tomato');
            $('html, body').animate({
                scrollTop: $(".redimportant").offset().top
            }, 200);
            return false;
        }

    })

    /***Slider  Article**/
    $(".addSliderOrImage").click(function() {
        var idCollap = makeid()
        var type = $(this).data('type');
        // var count = $('.'+type+'-box').length;
        var count = $('.' + type + '-box').length;
        var label = $(this).data("label");
        var prototype = $(this).data('prototype');
        var indexParent = count;
        var newForm = prototype.replace(/__name__/g, indexParent);
        $(this).data('index', indexParent + 1);
        var childItem;
        if ($(this).data("child") != undefined) {
            var childName = $(this).data("child");
            var childLabel = $(this).data("childlabel");
            var childPrototype = $(this).data("childprototype")
            var newFormChild = childPrototype.replace(/__name__/g, indexParent);
            childItem = '<a class="btn btn-primary btnAddChild" ' +
                'data-label="' + childLabel + '" data-type="' + childName + '"' +
                'data-prototype=\'' + newFormChild + ' \'>Ajouter ' + childLabel + '</a>';
            newForm = newForm + '<div class="childrenPast"></div>' + childItem
        }
        $('.sliderOrimageFiles').append(newForm);
        $(this).attr('disabled', 'disabled')
        $(".removeSliderOrImage").show()
        $(this).css('opacity', 0.6)
        return false
    })
    $(".removeSliderOrImage").click(function() {
            $(this).hide()
            $('.addSliderOrImage').css('opacity', 1)
            $('.addSliderOrImage').attr('disabled', false)
            $('.row.sliderOrimageFiles').empty()
            return false
        })
        /***SCollapsePanelChild**/
    $('body').on('click', '.CollapsePanelChild', function() {
        $(this).find('i.fa').toggleClass("fa-chevron-down fa-chevron-up")
        $(this).parent().toggleClass('childClosedPanel', 400)
    })

    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }
    $('body').on('change', 'input:file', function(event) {
            var reader = new FileReader();
            var img = document.createElement("img");
            var imageUrl;
            if ($(this).parent().find('.DisplayImageAfterUpload').length == 0) {
                $(this).parent().append('<div class="DisplayImageAfterUpload"></div>')
            }
            var output = $(this).parent().find('.DisplayImageAfterUpload');
            reader.onload = function() {
                img.src = reader.result;
                output.append(img)
                localStorage.setItem('item', imageUrl)
            }
            output.toggle('slow')
            reader.readAsDataURL(event.target.files[0]);

        })
        /** Parent **/
    $('body').on('click', '.addSingleContentButtun', function() {
            var idCollap = makeid()
            var type = $(this).data('type');
            // var count = $('.' + type + '-box').length;
            var count = $('.' + type + '-box').length;
            var label = $(this).data("label");
            console.log(label)
            var prototype = $(this).data('prototype');
            var indexParent = count;
            var newForm = prototype.replace(/__name__/g, indexParent);
            $(this).data('index', indexParent + 1);
            var childItem;
            if ($(this).data("child") != undefined) {
                var childName = $(this).data("child");
                var childLabel = $(this).data("childlabel");
                var childPrototype = $(this).data("childprototype")
                var newFormChild = childPrototype.replace(/__name__/g, indexParent);
                childItem = '<a class="btn btn-primary btnAddChild" ' +
                    'data-label="' + childLabel + '" data-type="' + childName + '"' +
                    'data-prototype=\'' + newFormChild + ' \'>Ajouter ' + childLabel + '</a>';
                newForm = newForm + '<div class="childrenPast"></div>' + childItem
            }

            var daat = '<div class="panel" id="panel">' +
                '<a class="panel-heading collapsed" role="tab" id="heading_' + idCollap + '_' + type +
                '" data-toggle="collapse"' +
                'data-parent="#accordion" href="#collapse_' + idCollap + '_' + type + '" aria-expanded="false" ' +
                'aria-controls="collapse_' + idCollap + '_' + type + '">' +
                '<h4 class="panel-title">' + label + '<span class="closeThisPanel"> X </span></h4>' +
                '</a>' +
                '<div id="collapse_' + idCollap + '_' + type + '" class="panel-collapse collapse" role="tabpanel"' +
                'aria-labelledby="heading_' + idCollap + '_' + type + '">' +
                '<div class="panel-body">' +
                newForm +
                '</div>' +
                '</div>' +
                '</div>';
            if (label == 'Image') {

                var imageLinkContent = 'class="image_link_to_get_from_others_inputs form-control" /></div>' +
                    '<div class="links_different_choice">' +
                    '<div class="radio">' +
                    ' <label>' +
                    '<input type="radio" class="radioInputChoice" name="optradio" checked>Lien Externe' +
                    '</label>' +
                    '<input type="link" class="form-control linkHideShow link_input_interne" style="display:block" placeholder="Lien Externe">' +
                    '</div>' +
                    '<div class="radio">' +
                    '<label>' +
                    '<input type="radio" class="radioInputChoice" name="optradio" >Lien Interne' +
                    '</label>' +
                    '<div class="form-group">' +
                    '<select class="form-control linkHideShow selecting">' +
                    ' <optgroup label="Pages" class="pages_all">' +
                    '</optgroup>' +
                    '<optgroup label="Articles" class="articles_all">' +
                    ' </optgroup>' +
                    '</select>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                daat = daat.replace('class="image_link_to_get_from_others_inputs form-control"></textarea></div>', imageLinkContent);
            }

            $('.accordion').append(daat);
            if (label == 'Image') {
                // all_articles_function($('.accordion').find('.panel').last())
                // all_pages_function($('.accordion').find('.panel').last())
            }

            if ($('.accordion').find(".panel:last").find('input:file')) {
                var afterFileIinput = '<div class="DisplayImageAfterUpload"></div>';
                $('.accordion').find(".panel:last").find('input:file').after(afterFileIinput);
            }

            $('.accordion').find('.panel').each(function(index) {
                var position = $(this).index();
                $(this).find('.contentPosition').val(position)

            });
            $('.accordion').sortable({
                stop: function(event, ui) {
                    $('.accordion').find('.panel').each(function(index) {
                        var position = $(this).index();
                        $(this).find('.contentPosition').val(position)
                    });
                },
            });
        })
        /** Parent Sortble **/
    $('.accordion').sortable({
        stop: function(event, ui) {
            $('.accordion').find('.panel').each(function(index) {
                var position = $(this).index();
                $(this).find('.contentPosition').val(position)
            });
        },
    });
    /** Child Sortble **/
    $('.childrenPast').sortable({
        stop: function(event, ui) {
            $(this).parent().find('.alert').each(function(index) {
                var position = $(this).index();
                $(this).find('.contentChildPosition').val(position)
            });
        },
    });
    /** Child **/
    $('body').on('click', '.btnAddChild', function() {
            var type = $(this).data('type');
            var count = $(this).parent().find('.childrenPast').find('.alert').length
            var label = $(this).data("label");
            var prototype = $(this).data('prototype');
            var newFormChild = prototype.replace(/__child_prot__/g, count);
            var finalINput = '<div class="alert alert-bepole alert-dismissible fade in" role="alert">' +
                '<button type="button" class="close closeChildEnitity" data-dismiss="alert" aria-label="Close"><span' +
                'aria-hidden="true">×</span>' + '</button>' +
                '<span class="CollapsePanelChild"> <i class="fa fa-chevron-down"></i> </span>' +
                '<h4>' + label + '</h4> <hr class="HrBeplesSup">' +
                newFormChild +
                '</div>';

            if (label == 'Image') {

                var imageLinkContent = 'class="image_link_to_get_from_others_inputs form-control" /></div>' +
                    '<div class="links_different_choice">' +
                    '<div class="radio">' +
                    ' <label>' +
                    '<input type="radio" class="radioInputChoice" name="optradio" checked>Lien Externe' +
                    '</label>' +
                    '<input type="link" class="form-control linkHideShow link_input_interne" style="display:block" placeholder="Lien Externe">' +
                    '</div>' +
                    '<div class="radio">' +
                    '<label>' +
                    '<input type="radio" class="radioInputChoice" name="optradio" >Lien Interne' +
                    '</label>' +
                    '<div class="form-group">' +
                    '<select class="form-control linkHideShow selecting">' +
                    ' <optgroup label="Pages" class="pages_all">' +
                    '</optgroup>' +
                    '<optgroup label="Articles" class="articles_all">' +
                    ' </optgroup>' +
                    '</select>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                finalINput = finalINput.replace('class="image_link_to_get_from_others_inputs form-control" /></div>', imageLinkContent);
            }

            $(this).parent().find('.childrenPast').first().append(finalINput);
            all_articles_function($(this).parent().find('.childrenPast').find('.alert').last())
            all_pages_function($(this).parent().find('.childrenPast').find('.alert').last())
            if ($(this).parent().find('.childrenPast').find(".alert").find('input:file')) {
                var afterFileIinput = '<div class="DisplayImageAfterUpload"></div>';
                $(this).parent().find('.childrenPast').find(".alert").find('input:file').after(afterFileIinput);
            }
            $('.childrenPast').find('.alert').each(function(index) {
                var position = $(this).index();
                $(this).find('.form-group').find('.contentChildPosition').val(position)
            });
            $('.childrenPast').sortable({
                stop: function(event, ui) {
                    $('.childrenPast').find('.alert').each(function(index) {
                        var position = $(this).index();
                        $(this).find('.form-group').find('.contentChildPosition').val(position)
                    });
                },
            });
        })
        /** Remove Parent **/
    $('body').on('click', '.closeThisPanel', function() {
            $(this).parent().parent().parent().remove()
            $('.accordion').find('.panel').each(function(index) {
                var position = $(this).index();
                $(this).find('.form-group').find('.contentPosition').val(position)
            });
        })
        /** Remove Child **/
    $('body').on('click', '.closeChildEnitity', function(e) {
        $(this).parent().remove()
        $(this).parent().parent().find('.alert').each(function(index) {
            var position = $(this).index();
            $(this).find('.form-group').find('.contentPosition').val(position)
        });
    })
    $('.form-group').each(function(index) {
        if ($(this).text() == '') {
            $(this).hide()
        }
    });

    /** LinkTest **/
    $('body').on('focus', '.radioInputChoice', function(e) {
        $(this).parent().parent().parent().find('.linkHideShow').each(function(index, el) {
            $(this).hide();
        });
        $(this).parent().parent().find('.linkHideShow').show();
    })

    $('body').on('click', '.radioInputChoice', function(e) {
        $(this).parent().parent().parent().find('.linkHideShow').each(function(index, el) {
            $(this).hide();
        });
        $(this).parent().parent().parent().find('label').each(function() {
            $(this).removeClass('active')
        })
        $(this).parent().addClass('active')
        $(this).parent().parent().find('.linkHideShow').show();


        $(this).parent().parent().parent().find('.image_link_to_get_from_others_inputs').val(' ')
        $(this).parent().parent().parent().find('.image_link_to_get_from_others_inputs').val($(this).parent().parent().find('select').val())
        console.log()
    })

    $('body').on('focus', '.linkHideShow', function(e) {
        if ($(this).hasClass('link_input_interne')) {
            $(this).keyup(function() {
                $(this).parent().parent().parent().find('.image_link_to_get_from_others_inputs').val($(this).val())
            });
        }
    })
    $('body').on('change', '.selecting', function(e) {
        $(this).parent().parent().parent().parent().find('.image_link_to_get_from_others_inputs').val($(this).val())
    })



})