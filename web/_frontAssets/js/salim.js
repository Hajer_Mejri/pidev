$(document).ready(function($) {
    var answersArray = [];
    if ($('body').find('.surveyPage').length > 0) {
        $('.radioBtnForJson').change(function(event) {
            var countIfExist = 0;
            var indexOfConcernedItemThatWillBeChanged;
            for (var i = answersArray.length - 1; i >= 0; i--) {
                if (answersArray[i].question_id == $(this).data('parent')) {
                    indexOfConcernedItemThatWillBeChanged = i;
                    countIfExist++;
                }
            }
            if (countIfExist > 0) {
                answersArray[indexOfConcernedItemThatWillBeChanged].answer_id = $(this).data('id')
            } else {
                answersArray.push({
                    'answer_id': $(this).data('id'),
                    'question_id': $(this).data('parent'),
                    'survey_id':$(this).data('survey')
                })
            }
            console.log(JSON.stringify(answersArray))
            $('textarea').val('')
            $('textarea').val(JSON.stringify(answersArray))
        });
    }
});