-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 16 juin 2019 à 15:40
-- Version du serveur :  5.7.24
-- Version de PHP :  7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `symfony_integration`
--

-- --------------------------------------------------------

--
-- Structure de la table `answers`
--

DROP TABLE IF EXISTS `answers`;
CREATE TABLE IF NOT EXISTS `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_50D0C6061E27F6BF` (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `content`, `score`) VALUES
(1, 1, 'Q1', 2),
(2, 1, 'Q2', 0),
(3, 2, 'R1', 1),
(4, 2, 'R2', 1);

-- --------------------------------------------------------

--
-- Structure de la table `client_type`
--

DROP TABLE IF EXISTS `client_type`;
CREATE TABLE IF NOT EXISTS `client_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `score` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `club`
--

DROP TABLE IF EXISTS `club`;
CREATE TABLE IF NOT EXISTS `club` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_club` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_club` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activity_club` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_club` longtext COLLATE utf8_unicode_ci,
  `price_club` double DEFAULT NULL,
  `openningHours_club` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `closingHours_club` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `sum_vote` double DEFAULT NULL,
  `nb_vote` int(11) DEFAULT NULL,
  `voters` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_B8EE3872A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `club`
--

INSERT INTO `club` (`id`, `name_club`, `category_club`, `activity_club`, `description_club`, `price_club`, `openningHours_club`, `closingHours_club`, `user_id`, `sum_vote`, `nb_vote`, `voters`) VALUES
(1, 'Club 1', 'Club C 1', 'Club CC1', '<h2 style=\"padding: 0px; font-weight: 400; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Qu\'est-ce que le Lorem Ipsum?</h2><p style=\"padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Le&nbsp;<strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong>&nbsp;est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.</p>', 20, '10', '20', 1, 4.5, 1, '[1]');

-- --------------------------------------------------------

--
-- Structure de la table `club_voucher`
--

DROP TABLE IF EXISTS `club_voucher`;
CREATE TABLE IF NOT EXISTS `club_voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `insertat` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1EDA005A61190A32` (`club_id`),
  KEY `IDX_1EDA005AA76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `club_voucher`
--

INSERT INTO `club_voucher` (`id`, `club_id`, `user_id`, `code`, `insertat`) VALUES
(1, 1, 1, 'cbab220c3da800f205e811934515e67a81afac52', '2019-06-16 17:17:40');

-- --------------------------------------------------------

--
-- Structure de la table `competition`
--

DROP TABLE IF EXISTS `competition`;
CREATE TABLE IF NOT EXISTS `competition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_id` int(11) NOT NULL,
  `name_competition` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visibility_competition` tinyint(1) NOT NULL,
  `startDate_competition` datetime DEFAULT NULL,
  `endDate_competition` datetime DEFAULT NULL,
  `description_competition` longtext COLLATE utf8_unicode_ci,
  `place_competition` longtext COLLATE utf8_unicode_ci,
  `contact_competition` longtext COLLATE utf8_unicode_ci,
  `media_id` int(11) NOT NULL,
  `insertat` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B50A2CB161190A32` (`club_id`),
  KEY `IDX_B50A2CB1EA9FDD75` (`media_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `competition`
--

INSERT INTO `competition` (`id`, `club_id`, `name_competition`, `visibility_competition`, `startDate_competition`, `endDate_competition`, `description_competition`, `place_competition`, `contact_competition`, `media_id`, `insertat`) VALUES
(1, 1, 'Comp 1', 1, '2019-09-01 00:00:00', '2020-01-01 00:00:00', '<h2 style=\"padding: 0px; font-weight: 400; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Qu\'est-ce que le Lorem Ipsum?</h2><p style=\"padding: 0px; text-align: justify; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Le&nbsp;<strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong>&nbsp;est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.</p>', 'tunis', '23290248', 2, '2019-06-16 17:16:47');

-- --------------------------------------------------------

--
-- Structure de la table `competition_user`
--

DROP TABLE IF EXISTS `competition_user`;
CREATE TABLE IF NOT EXISTS `competition_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `competition_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `insertat` datetime DEFAULT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_83D0485B7B39D312` (`competition_id`),
  KEY `IDX_83D0485BA76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prix` double NOT NULL,
  `heure` time NOT NULL,
  `date` date NOT NULL,
  `lieu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `fos_user`
--

DROP TABLE IF EXISTS `fos_user`;
CREATE TABLE IF NOT EXISTS `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `Nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Prenom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `Nom`, `Prenom`) VALUES
(1, 'salim', 'salim', 'salim@lightmyweb.fr', 'salim@lightmyweb.fr', 1, NULL, '$2y$13$.Wnlu6NNrV.kApZEzkLWkevzfkLaFy08gXuFNXKylDiwEMQXh3bti', '2019-06-16 17:06:42', NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `media`
--

DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insertat` datetime DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6A2CA10C61190A32` (`club_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `media`
--

INSERT INTO `media` (`id`, `club_id`, `name`, `insertat`, `path`) VALUES
(1, 1, 'hello', '2019-06-16 17:15:35', 'f65ed6369e92912a345ae7752c62f324.jpeg'),
(2, NULL, 'img_3.jpg', '2019-06-16 17:16:47', '3369883f8118eb60a243030f7b3c4386.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `mediap`
--

DROP TABLE IF EXISTS `mediap`;
CREATE TABLE IF NOT EXISTS `mediap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insertat` datetime DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `mediap`
--

INSERT INTO `mediap` (`id`, `name`, `insertat`, `path`) VALUES
(1, 'download', '2019-06-15 20:04:56', 'a77bda443b3658b903f239f68438c1a9.jpeg');

-- --------------------------------------------------------

--
-- Structure de la table `projection`
--

DROP TABLE IF EXISTS `projection`;
CREATE TABLE IF NOT EXISTS `projection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salle_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `Description_Projection` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Categorie_Projection` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Title_Projection` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Duree_Projection` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_Projection` date NOT NULL,
  `Prix_Projection` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8004C826DC304035` (`salle_id`),
  KEY `IDX_8004C8263DA5256D` (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `projection`
--

INSERT INTO `projection` (`id`, `salle_id`, `image_id`, `Description_Projection`, `Categorie_Projection`, `Title_Projection`, `Duree_Projection`, `date_Projection`, `Prix_Projection`) VALUES
(1, 1, 1, 'projection 1 description', 'FILM', 'projection 1', '20:30', '2019-06-12', 20);

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B6F7494EB3FE509D` (`survey_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `question`
--

INSERT INTO `question` (`id`, `survey_id`, `content`, `type`, `position`, `createdAt`) VALUES
(1, 1, 'Q1', NULL, 0, NULL),
(2, 1, 'Q2', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `salle`
--

DROP TABLE IF EXISTS `salle`;
CREATE TABLE IF NOT EXISTS `salle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom_salle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Etat_salle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Bloc_salle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NbreChaise_salle` int(11) NOT NULL,
  `Categorie_salle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `salle`
--

INSERT INTO `salle` (`id`, `Nom_salle`, `Etat_salle`, `Bloc_salle`, `NbreChaise_salle`, `Categorie_salle`) VALUES
(1, 'salle 1', 'NON DISPONIBLE', 'BLOC A', 47, 'Salle_Cinema');

-- --------------------------------------------------------

--
-- Structure de la table `survey`
--

DROP TABLE IF EXISTS `survey`;
CREATE TABLE IF NOT EXISTS `survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_AD5F9BFC12469DE2` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `survey`
--

INSERT INTO `survey` (`id`, `category_id`, `title`, `description`) VALUES
(1, 1, 'sondage 1', 'sondage 1 description');

-- --------------------------------------------------------

--
-- Structure de la table `survey_category`
--

DROP TABLE IF EXISTS `survey_category`;
CREATE TABLE IF NOT EXISTS `survey_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `survey_category`
--

INSERT INTO `survey_category` (`id`, `name`) VALUES
(1, 'Categorie 1');

-- --------------------------------------------------------

--
-- Structure de la table `survey_result`
--

DROP TABLE IF EXISTS `survey_result`;
CREATE TABLE IF NOT EXISTS `survey_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `json` longtext COLLATE utf8_unicode_ci,
  `survey` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `survey_result`
--

INSERT INTO `survey_result` (`id`, `email`, `name`, `json`, `survey`) VALUES
(1, 'salim_mejdoub@hotmail.com', 'Salim Mejdoub', '[{\"answer_id\":1,\"question_id\":1,\"survey_id\":1},{\"answer_id\":4,\"question_id\":2,\"survey_id\":1}]', 1),
(2, 'djase.dub@gmail.com', 'Salim Mejdoub', '[{\"answer_id\":1,\"question_id\":1,\"survey_id\":1},{\"answer_id\":3,\"question_id\":2,\"survey_id\":1}]', 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `FK_50D0C6061E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`);

--
-- Contraintes pour la table `club`
--
ALTER TABLE `club`
  ADD CONSTRAINT `FK_B8EE3872A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `club_voucher`
--
ALTER TABLE `club_voucher`
  ADD CONSTRAINT `FK_1EDA005A61190A32` FOREIGN KEY (`club_id`) REFERENCES `club` (`id`),
  ADD CONSTRAINT `FK_1EDA005AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `competition`
--
ALTER TABLE `competition`
  ADD CONSTRAINT `FK_B50A2CB161190A32` FOREIGN KEY (`club_id`) REFERENCES `club` (`id`),
  ADD CONSTRAINT `FK_B50A2CB1EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`);

--
-- Contraintes pour la table `competition_user`
--
ALTER TABLE `competition_user`
  ADD CONSTRAINT `FK_83D0485B7B39D312` FOREIGN KEY (`competition_id`) REFERENCES `competition` (`id`),
  ADD CONSTRAINT `FK_83D0485BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `FK_6A2CA10C61190A32` FOREIGN KEY (`club_id`) REFERENCES `club` (`id`);

--
-- Contraintes pour la table `projection`
--
ALTER TABLE `projection`
  ADD CONSTRAINT `FK_8004C8263DA5256D` FOREIGN KEY (`image_id`) REFERENCES `mediap` (`id`),
  ADD CONSTRAINT `FK_8004C826DC304035` FOREIGN KEY (`salle_id`) REFERENCES `salle` (`id`);

--
-- Contraintes pour la table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `FK_B6F7494EB3FE509D` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`);

--
-- Contraintes pour la table `survey`
--
ALTER TABLE `survey`
  ADD CONSTRAINT `FK_AD5F9BFC12469DE2` FOREIGN KEY (`category_id`) REFERENCES `survey_category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
